<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class LoginController extends Controller
{
    public function viewAllUser()
    {
        $allUsers = User::all();

        return response()->json($allUsers);
    }

    public function findUser($username)
    {
        $user = User::where('username', $username)->first();

        return response()->json($user);
    }

    public function loginAction(Request $request){
        $validatedData = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $userDB = User::where('username', $request->username)->first();

        if($userDB){
            // check password
            if(Hash::check($request->password, $userDB->password)){
                $request->session()->put('id_pengguna', $userDB->id);
                $request->session()->put('name', $userDB->name);
                $request->session()->put('kategori_pengguna', $userDB->kategori_pengguna);
                return response()->json([
                    'message' => 'success'
                ]);
            }else{
                return response()->json([
                    'message' => 'err_pass'
                ]);
            }
        }else{
            return response()->json([
                'message' => 'err_user'
            ]);
        }

    }

    public function logoutAction(){
        Session::flush();
        return redirect('/');
    }
}
