<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\StatusPesanan;
use App\Pesanan;
use App\User;

class PesananController extends Controller
{
    public function tambahPesanan(Request $request){
        $statusPesanan = new StatusPesanan;

        $nomorPesananDummy = "1234";
        $statusPesananDefault = "aktif";

        // Insert New Status Pesanan to get the ID
        $statusPesanan->nomor_meja = $request->nomor_meja;
        $statusPesanan->nomor_pesanan = $nomorPesananDummy;
        $statusPesanan->id_pengguna = $request->id_pengguna;
        $statusPesanan->status_pesanan = $statusPesananDefault;
        $statusPesanan->save();

        // get Inserted ID
        $statusPesananID = $statusPesanan->id;

        // generate Nomor Pesanan
        $tanggal = gmdate('d');
        $bulan = gmdate('m');
        $tahun = gmdate('Y');
        $number = sprintf('%03d',$statusPesananID);

        $nomorPesanan = "ERP".$tanggal."".$bulan."".$tahun."-".$number;

        // update Nomor Pesanan
        $statusPesananInserted = StatusPesanan::find($statusPesananID);
        $statusPesananInserted->nomor_pesanan = $nomorPesanan;
        $statusPesananInserted->save();

        // insert Pesanan to table Pesanan
        $idMenus = $request->input('id_menu');
        foreach($idMenus as $idMenu){
            $pesanan = new Pesanan;
            $pesanan->id_pesanan = $statusPesananID;
            $pesanan->id_menu = $idMenu;
            $pesanan->save();
        }

        return response()->json([
            'message_pesanan' => 'success'
        ]);
    }

    public function viewDetailPesanan($id){
        $namaMenus = [];
        $index = 0;
        $totalHarga = 0;
        $pesananDetails = Pesanan::where('id_pesanan', $id)->get();
        foreach ($pesananDetails as $pesananDetail) {
            $menuGet = Menu::where('id', $pesananDetail->id_menu)->first();
            $namaMenu = $menuGet->nama;
            $hargaMenu = $menuGet->harga;
            $totalHarga = $hargaMenu + $totalHarga;

            $namaMenus[$index]['nama'] = $namaMenu;
            $index++;
        }

        // convert it to Rupiah
        $totalHargaFloat = floatval($totalHarga);
        $totalHargaRes = number_format($totalHargaFloat, 0, ".", ".");
        $totalHargaRes = "Rp ".$totalHargaRes;

        return response()->json(array(
            "code" => 1,
            "data"  => $namaMenus,
            "totalHarga" => $totalHargaRes
        ));
    }

    public function viewUbahPesanan($id)
    {
        $statusPesanan = new statusPesanan;
        $pesanan = new Pesanan;
        $statusPesananArr = [];
        $index = 0;
        $totalHarga = 0;

        // find Status Pesanan
        $findStatusPesanan = $statusPesanan::find($id);

        // find detail Pesanan
        $pesananDetails = $pesanan::where('id_pesanan', $id)->get();
        foreach ($pesananDetails as $pesananDetail) {
            // $menuGet = Menu::where('id', $pesananDetail->id_menu)->first();
            // $namaMenu = $menuGet->nama;
            // $hargaMenu = $menuGet->harga;
            // $totalHarga = $hargaMenu + $totalHarga;

            $namaMenus[$index]['id_menu'] = $pesananDetail->id_menu;
            $namaMenus[$index]['id_detail_pesanan'] = $pesananDetail->id;
            $index++;
        }

        $nomor_meja = $findStatusPesanan->nomor_meja;
        $nomor_pesanan = $findStatusPesanan->nomor_pesanan;
        $id_pengguna = $findStatusPesanan->id_pengguna;
        $status_pesanan = $findStatusPesanan->status_pesanan;

        // get Nama Pengguna
        $userInfo = User::where('id', $id_pengguna)->first();
        $nama_pengguna = $userInfo->name;
        $kategori_pengguna = $userInfo->kategori_pengguna;

        // push to array status pesanan
        $statusPesananArr['id_status_pesanan'] = $id;
        $statusPesananArr['nomor_meja'] = $nomor_meja;
        $statusPesananArr['nomor_pesanan'] = $nomor_pesanan;
        $statusPesananArr['nama_pengguna'] = $nama_pengguna;
        $statusPesananArr['kategori_pengguna'] = $kategori_pengguna;
        $statusPesananArr['status_pesanan'] = $status_pesanan;

        return response()->json([
            'status_pesanan' => $statusPesananArr,
            'detail_pesanan' => $namaMenus
        ]);

    }

    public function ubahPesananAction(Request $request)
    {

        $statusPesanan = StatusPesanan::where('id', $request->id_status_pesanan)->first();

        $statusPesanan->nomor_meja = $request->nomor_meja;
        $statusPesanan->nomor_pesanan = $request->nomor_pesanan;
        $statusPesanan->save();

        // delete Pesanan on table Pesanan
        $pesananOlds = Pesanan::where('id_pesanan', $request->id_status_pesanan)->get();
        foreach ($pesananOlds as $pesananOld) {
            $pesananOld->delete();
        }

        // insert Pesanan to table Pesanan
        $idMenus = $request->input('id_menu');
        foreach($idMenus as $idMenu){
            $pesanan = new Pesanan;
            $pesanan->id_pesanan = $request->id_status_pesanan;
            $pesanan->id_menu = $idMenu;
            $pesanan->save();
        }

        return response()->json([
            'ubah_status' => 'success'
        ]);
    }

    public function hapusAction($id)
    {
        $statusPesanan = StatusPesanan::where('id', $id)->first();

        // delete Pesanan on table Pesanan
        $pesananOlds = Pesanan::where('id_pesanan', $id)->get();
        foreach ($pesananOlds as $pesananOld) {
            $pesananOld->delete();
        }

        $statusPesanan->delete();

        return redirect('/home-user');
    }

    public function bayarPesananAction($id)
    {
        $statusPesanan = StatusPesanan::where('id', $id)->first();

        $bayarStatus = "paid";

        $statusPesanan->status_pesanan = $bayarStatus;
        $statusPesanan->save();

        return redirect('/home-user');
    }
}
