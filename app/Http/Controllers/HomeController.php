<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\StatusPesanan;

class HomeController extends Controller
{
    public function viewHome(){
        return view('home.home');
    }

    public function viewHomeUser(){
        $menus = Menu::all();
        $pesananData = StatusPesanan::all();
        return view('home-user.home-user', compact(['menus', 'pesananData']));
    }
}
