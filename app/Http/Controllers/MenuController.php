<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;

class MenuController extends Controller
{
    public function viewAllMenu()
    {
        $allMenus = Menu::all();

        return response()->json($allMenus);
    }

    public function findMenu($id)
    {
        $menu = Menu::find($id);

        return response()->json($menu);
    }

    public function tambahMenu(Request $request){
        $validatedData = $request->validate([
            'nama' => 'required|unique:menu',
            'jenis' => 'required',
            'harga' => 'required|numeric',
        ]);

        $statusDefault = 'not_ready';

        $menu = new Menu;

        $menu->nama = $request->nama;
        $menu->jenis = $request->jenis;
        $menu->harga = $request->harga;
        $menu->status = $statusDefault;

        if($menu->save()){
            return response()->json([
                'message_menu' => 'success'
            ]);
        }else{
            return response()->json([
                'message_menu' => 'error'
            ]);
        }
    }

    public function viewUbahMenu($id)
    {
        $menu = new Menu;
        $viewMenu = $menu::find($id);

        $nama = $viewMenu->nama;
        $jenis = $viewMenu->jenis;
        $harga = $viewMenu->harga;
        $status = $viewMenu->status;

        return response()->json([
            'id_menu' => $id,
            'nama' => $nama,
            'jenis' => $jenis,
            'harga' => $harga,
            'status' => $status,
        ]);
    }

    public function ubahMenuAction(Request $request)
    {
        $menu = new Menu;

        $id = $request->id_menu;

        // find Menu
        $findMenu = $menu::find($id);

        $findMenu->nama = $request->nama;
        $findMenu->jenis = $request->jenis;
        $findMenu->harga = $request->harga;
        $findMenu->status = $request->status;

        if ($findMenu->save()) {
            return response()->json([
                'status' => 'success'
            ]);
        }
    }

    public function hapusAction($id){
        $menu = new Menu;

        // find Menu
        $findMenu = $menu::find($id);

        // delete Menu
        if ($findMenu->delete()) {
            return redirect('/home-user')->with(['delete_status' => 'success']);
        }else{
            return redirect('/home-user')->with(['delete_status' => 'failed']);
        }
    }
}
