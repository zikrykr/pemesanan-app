<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class RegisterController extends Controller
{
    public function registerNewUser(Request $request){
        $validatedData = $request->validate([
            'username' => 'required|unique:users',
            'name' => 'required',
            'password' => 'required',
            'kategori_pengguna' => 'required',
        ]);

        $hashed = Hash::make($request->password, [
            'memory' => 1024,
            'time' => 2,
            'threads' => 2,
        ]);

        $user = new User;

        $user->username = $request->username;
        $user->name = $request->name;
        $user->password = $hashed;
        $user->kategori_pengguna = $request->kategori_pengguna;
        $user->save();

        return response()->json([
            'request_message' => 'success'
        ]);
    }
}
