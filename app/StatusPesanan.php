<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusPesanan extends Model
{
    protected $table = "status_pesanan";

    protected $fillable = ['nomor_pesanan', 'nomor_meja', 'id_pengguna', 'status_pesanan'];

}
