## Note To Install

- Clone the file from repository or by downloading as zip
- Import the sql file to database from db folder (for configuration you can check it on .env file)

## Existing User For Login (or you can register yourself)

**Kasir** : 
- username = 'kasir'
- password = 'kasir'

**Pelayan** : 
- username = 'pelayan'
- password = 'pelayan'


## API Address

**User**
- Get All Users : '/api/users' (GET Request)
- Get User by Username : '/api/user/{username}' (GET Request)

**Menu**
- Get All Menus : '/api/menus' (GET Request)
- Get Menu by ID : 'api/menu/{id}' (GET Request)