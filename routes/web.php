<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@viewHome')->middleware('loggedIn');

// Login Register and Logout
Route::post('/register', 'RegisterController@registerNewUser');
Route::post('/login', 'LoginController@loginAction');
Route::get('/logout', 'LoginController@logoutAction');

// view Home User Controller
Route::get('/home-user', 'HomeController@viewHomeUser')->middleware('notloggedIn');

// Menu Controller
Route::post('/menu/tambah', 'MenuController@tambahMenu');
Route::get('/menu/ubah/{id}', 'MenuController@viewUbahMenu');
Route::post('/menu/ubah', 'MenuController@ubahMenuAction');
Route::get('/menu/hapus/{id}', 'MenuController@hapusAction');

// Pesanan Controller
Route::post('/pesanan/tambah', 'PesananController@tambahPesanan');
Route::get('/pesanan/detail/{id}', 'PesananController@viewDetailPesanan');
Route::get('/pesanan/ubah/{id}', 'PesananController@viewUbahPesanan');
Route::post('/pesanan/ubah', 'PesananController@ubahPesananAction');
Route::get('/pesanan/hapus/{id}', 'PesananController@hapusAction');
Route::get('/pesanan/bayar/{id}', 'PesananController@bayarPesananAction');
