@extends('layout')

@section('css')
    @include('home._style.home')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div id="titleApp" class="col-lg-5 col-12 vh-100 d-flex align-items-center justify-content-center bg-blue">
                <h3 style="color:#fff;">Pemesanan App</h3>
            </div>
            <div class="col-lg-7 col-12 vh-100 d-flex align-items-center justify-content-center">
                <div class="row">
                    <div class="col-12 mx-auto login-box p-4">
                        {{-- alert --}}
                        <div id="alert" class="alert alert-dismissible fade show d-none" role="alert">
                            <span id="alertMsg"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="text-center mb-4">
                            <h5 class="font-weight-bold">Login</h5>
                        </div>
                        <form id="loginForm" action="/login" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" class="form-control" name="username">
                            </div>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                            <button type="submit" id="loginBtn" class="btn btn-primary w-100">Login</button>
                        </form>
                        <div class="d-flex align-items-center justify-content-between mt-4">
                            <a href="#">Lupa Password?</a>
                            <a href="#" data-toggle="modal" data-target="#modalsDaftar">Daftar Akun</a>
                        </div>
                        <div id="test">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('home._components.modals.daftar')
@endsection

@section('js')
    @include('home._js.home')
@endsection
