<script>

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
})

// Register New User Ajax
$('#registerForm').submit(function(e){
    e.preventDefault();

    var formData = new FormData($(this)[0]);

    $.ajax({
        url: '/register',
        type: 'POST',
        data: formData,
        beforeSend: function(){
            $("#daftarBtn").html("Mendaftar <i class='fas fa-spinner fa-spin'></i>");
        },
        success: function(data){
            if(data.request_message == "success"){
                $("#modalsDaftar").modal('hide');
                if($("#alert").hasClass('alert-danger')){
                    $("#alert").removeClass('alert-danger');
                }
                $("#alert").addClass('alert-success');
                $("#alertMsg").html("Berhasil mendaftar");
                $("#alert").removeClass('d-none');
                setTimeout(function(){
                    location.reload();
                }, 1000);
            }
        },
        error: function(reject){
            if( reject.status === 422 ) {
                var errors = $.parseJSON(reject.responseText);
                $("#test").html(errors);
                $.each(errors, function(i, error){
                    $("#modalsDaftar").modal('hide');
                    $("#alert").addClass('alert-danger');
                    $("#alertMsg").html(errors[i].username);
                    $("#alert").removeClass('d-none');
                    $("#daftarBtn").html("Daftar");
                    $("#modalsDaftar").hide();
                });
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

// Login Ajax
$("#loginForm").submit(function(e){
    e.preventDefault();

    var formDataLogin = new FormData($(this)[0]);

    $.ajax({
        url: '/login',
        type: 'POST',
        data: formDataLogin,
        beforeSend: function(){
            $("#loginBtn").html("Melakukan login <i class='fas fa-spinner fa-spin'></i>");
        },
        success: function(data){
            if(data.message == "success"){
                if($("#alert").hasClass('alert-danger')){
                    $("#alert").removeClass('alert-danger');
                }
                $("#alert").addClass('alert-success');
                $("#alertMsg").html("Log in berhasil!");
                $("#alert").removeClass('d-none');
                window.location.href="/home-user";
            }

            if(data.message == "err_pass"){
                if($("#alert").hasClass('alert-danger')){
                    $("#alert").removeClass('alert-danger');
                }
                $("#alert").addClass('alert-danger');
                $("#alertMsg").html("Password salah!");
                $("#alert").removeClass('d-none');
            }
            if(data.message == "err_user"){
                if($("#alert").hasClass('alert-danger')){
                    $("#alert").removeClass('alert-danger');
                }
                $("#alert").addClass('alert-danger');
                $("#alertMsg").html("Username tidak ditemukan!");
                $("#alert").removeClass('d-none');
            }

            setTimeout(function(){
                location.reload();
            }, 1000);
        },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>
