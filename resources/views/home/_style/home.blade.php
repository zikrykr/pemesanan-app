<style>
    .vh-100{
        height: 100vh;
    }

    .bg-blue{
        background: #37cece;
    }

    .login-box{
        width: 100%;
        border: 1px solid #ccc;
        border-radius: 10px;
    }

    @media screen and (max-width: 600px){
        #titleApp{
            display: none !important;
        }
    }
</style>
