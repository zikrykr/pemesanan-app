<div class="modal fade" id="modalsDaftar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="font-weight-bold text-center">Daftar Akun</h5>
                <div class="mt-4">
                    <form action="/register" id="registerForm" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" class="form-control" name="username">
                        </div>
                        <div class="form-group">
                            <label for="">Nama</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                            <label for="Kategori Pegawai">Kategori Pegawai</label><br>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" name="kategori_pengguna" value="pelayan" id="">
                                <label for="" class="form-check-label">Pelayan</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" name="kategori_pengguna" value="kasir" id="">
                                <label for="" class="form-check-label">Kasir</label>
                            </div>
                        </div>
                        <button id="daftarBtn" type="submit" class="btn btn-primary w-100">Daftar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
