@extends('layout')
@section('css')
    @include('home-user._style.home-user')
@endsection

@section('content')
@if (!empty($modal) && $modal == 'open')
    <script>
        $(function(){
            $('#modalsDetailPesanan').modal('show');
        });
    </script>
@endif
    <div class="container">
        <div class="row mt-4">
            <div class="col-12 d-flex align-items-center justify-content-between mb-5">
                <h4 class="font-weight-bold mb-0">Pemesanan App</h4>
                <div class="d-flex align-items-center">
                    <p class="mb-0 mr-4 text-capitalize">Pengguna : {{Session::get('name')}} ( {{Session::get('kategori_pengguna')}} )</p>
                    <a href="/logout" class="btn btn-danger">Log out</a>
                </div>
            </div>
            @if (!empty(Session::get('delete_status')) && Session::get('delete_status') == "success")
            <div class="alert alert-success alert-dismissible fade show d-none" role="alert">
                Berhasil menghapus menu!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if (!empty($delete_status) && $delete_status == "fail")
            <div class="alert alert-danger alert-dismissible fade show d-none" role="alert">
                Terjadi Kesalahan!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            {{-- Daftar Menu START --}}
            <div class="col-12 mb-5" style="border-bottom: 1px solid #ccc;">
                <div class="d-flex align-items-center justify-content-between mb-3">
                    <div class="d-flex align-items-center">
                        <h5 class="mr-3 mb-0">Daftar Menu</h5>
                        <i class="fas fa-chevron-up table-toggle"></i>
                    </div>
                    <button type="button" data-toggle="modal" data-target="#modalsTambahMenu" class="btn btn-primary"><i class="fas fa-plus"></i> &nbsp;Tambah Menu</button>
                </div>
                <div class="table-responsive-sm">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th scope="col">No.</th>
                                <th scope="col">Nama Menu</th>
                                <th scope="col">Jenis Menu</th>
                                <th scope="col">Harga</th>
                                <th scope="col">Status</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($menus->isEmpty())
                                <tr class="text-center pt-4">
                                    <td class="pt-4 font-weight-bold" colspan="6">Daftar Menu Kosong</td>
                                </tr>
                            @else
                                @foreach ($menus as $menu)
                                    @php
                                        $noMenu = 1;
                                    @endphp
                                    @if ($menu->status == "not_ready")
                                        @php
                                            $statusDisplay = 'Not Ready';
                                        @endphp
                                    @else
                                        @php
                                            $statusDisplay = "Ready";
                                        @endphp
                                    @endif
                                    <tr class="text-center">
                                        <td style="vertical-align:middle;">{{$noMenu}}</td>
                                        <td style="vertical-align:middle;" class="text-capitalize">{{$menu->nama}}</td>
                                        <td style="vertical-align:middle;" class="text-capitalize">{{$menu->jenis}}</td>
                                        <td style="vertical-align:middle;">{{$menu->harga}}</td>
                                        <td style="vertical-align:middle;" class="text-capitalize">{{$statusDisplay}}</td>
                                        <td style="vertical-align:middle;" class="text-center d-flex align-items-center justify-content-center">
                                            <button type="button" data-toggle="modal" data-target="#modalsUbahMenu" onclick="getViewUbahMenu({{$menu->id}})" class="btn btn-warning mr-2">Ubah</button>
                                            <button type="button" data-toggle="modal" data-target="#modalsDeleteMenuConfirmation" onclick="getViewDeleteMenu({{$menu->id}})" class="btn btn-danger">Hapus</button>
                                        </td>
                                    </tr>
                                    @php
                                        $noMenu++;
                                    @endphp
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- Daftar Menu END --}}

            {{-- Daftar Pesanan START --}}
            <div class="col-12 mb-5" style="border-bottom: 1px solid #ccc;">
                <div class="d-flex align-items-center justify-content-between mb-3">
                    <div class="d-flex align-items-center">
                        <h5 class="mr-3 mb-0" style=>Daftar Pesanan</h5>
                        <i class="fas fa-chevron-up table-toggle"></i>
                    </div>
                    <button type="button" data-toggle="modal" data-target="#modalsTambahPesanan" class="btn btn-primary"><i class="fas fa-plus"></i> &nbsp;Tambah Pesanan</button>
                </div>
                <div class="table-responsive-sm">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th scope="col">No.</th>
                                <th scope="col">No. Pesanan</th>
                                <th scope="col">No. Meja</th>
                                <th scope="col">Status</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($pesananData->isEmpty())
                                <tr class="text-center pt-4">
                                    <td class="pt-4 font-weight-bold" colspan="6">Daftar Pesanan Kosong</td>
                                </tr>
                            @else
                                @foreach ($pesananData as $pesanan)
                                    <tr class="text-center">
                                        <td style="vertical-align:middle;">1</td>
                                        <td style="vertical-align:middle;" class="text-capitalize">{{$pesanan->nomor_pesanan}}</td>
                                        <td style="vertical-align:middle;" class="text-capitalize">{{$pesanan->nomor_meja}}</td>
                                        <td style="vertical-align:middle;">{{$pesanan->status_pesanan}}</td>
                                        <td style="vertical-align:middle;" class="text-center d-flex align-items-center justify-content-center">
                                            @if ($pesanan->status_pesanan != "paid")
                                                <button type="button" data-toggle="modal" data-target="#modalsUbahPesanan" onclick="getViewUbahPesanan({{$pesanan->id}})" class="btn btn-warning mr-2">Ubah</button>
                                                <button type="button" data-toggle="modal" data-target="#modalsDeletePesananConfirmation" onclick="getViewDeletePesanan({{$pesanan->id}})" class="btn btn-danger mr-2">Hapus</button>
                                                @if (Session::get('kategori_pengguna') == "kasir")
                                                <a href="/pesanan/bayar/{{$pesanan->id}}" class="btn btn-info mr-2">Bayar</a>
                                                @endif
                                            @endif
                                            <button type="button" data-toggle="modal" data-target="#modalsDetailPesanan" onclick="getDetailPesanan({{$pesanan->id}})" class="btn btn-success">Detail</button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- Daftar Pesanan END --}}

            {{-- Daftar Pesanan Saya START --}}
            <div class="col-12 mb-5" style="border-bottom: 1px solid #ccc;">
                <div class="d-flex align-items-center justify-content-between mb-3">
                    <div class="d-flex align-items-center">
                        <h5 class="mr-3 mb-0" style=>Daftar Pesanan Saya</h5>
                        <i class="fas fa-chevron-up table-toggle"></i>
                    </div>
                    <button type="button" data-toggle="modal" data-target="#modalsCetakPesanan" class="btn btn-primary"><i class="fas fa-print"></i> &nbsp;Cetak Pesanan</button>
                </div>
                <div class="table-responsive-sm">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th scope="col">No.</th>
                                <th scope="col">No. Pesanan</th>
                                <th scope="col">No. Meja</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($pesananData->isEmpty())
                                <tr class="text-center pt-4">
                                    <td class="pt-4 font-weight-bold" colspan="6">Daftar Pesanan Kosong</td>
                                </tr>
                            @else
                                @foreach ($pesananData as $pesanan)
                                    @if ($pesanan->id_pengguna == Session::get('id_pengguna'))
                                        <tr class="text-center">
                                            <td style="vertical-align:middle;">1</td>
                                            <td style="vertical-align:middle;" class="text-capitalize">{{$pesanan->nomor_pesanan}}</td>
                                            <td style="vertical-align:middle;" class="text-capitalize">{{$pesanan->nomor_meja}}</td>
                                            <td style="vertical-align:middle;">{{$pesanan->status_pesanan}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- Daftar Pesanan Saya END --}}

        </div>
    </div>

    @include('home-user._components.modals.tambahMenu')
    @include('home-user._components.modals.tambahPesanan')
    @include('home-user._components.modals.detailPesanan')
    @include('home-user._components.modals.ubahMenu')
    @include('home-user._components.modals.deleteMenuConfirmation')
    @include('home-user._components.modals.deletePesananConfirmation')
    @include('home-user._components.modals.ubahPesanan')
@endsection

@section('js')
    @include('home-user._js.home-user')
@endsection
