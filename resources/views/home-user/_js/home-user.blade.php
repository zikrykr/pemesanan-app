<script>

var alert = $("#alert");
var alertMsg = $("#alertMsg");
var alertPesanan = $("#alertPesanan");
var alertPesananMsg = $("#alertPesananMsg");
var alertUbahPesanan = $("#alertUbahPesanan");
var alertUbahPesananMsg = $("#alertUbahPesananMsg");
var alertUbahMenu = $("#alertUbahMenu");
var alertUbahMenuMsg = $("#alertUbahMenuMsg");

// Tambah Menu Ajax
$("#tambahMenuForm").submit(function(e){
    e.preventDefault();

    var formDataMenu = new FormData($(this)[0]);

    $.ajax({
        url: '/menu/tambah',
        type: 'POST',
        data: formDataMenu,
        beforeSend: function(data){
            $("#tambahMenuBtn").html('Menambah <i class="fas fa-spinner fa-spin"></i>');
        },
        success: function(data){
            if(data.message_menu == "success"){
                alert.addClass('alert-success');
                alertMsg.html("Berhasil menambah menu!");
                alert.removeClass('d-none');
            }else{
                alert.addClass('alert-danger');
                alertMsg.html("Terjadi Kesalahan!");
                alert.removeClass('d-none');
            }
            setTimeout(function(){
                location.reload();
            }, 1000);
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

// toggle table display
var tableToggleBtn = document.getElementsByClassName('table-toggle');
var i;

for( i = 0; i < tableToggleBtn.length; i++){
    tableToggleBtn[i].addEventListener("click", function(){
        var tableToggleParent = this.parentElement;
        var outerParent = tableToggleParent.parentElement;
        var tableElement = outerParent.nextElementSibling;

        // toggle icon on button
        if(this.classList.contains('fa-chevron-down')){
            this.classList.remove('fa-chevron-down');
            this.classList.add('fa-chevron-up');
        }else{
            this.classList.add('fa-chevron-down');
            this.classList.remove('fa-chevron-up');
        }

        // toggle table display
        tableElement.classList.toggle('d-none');
    });
}

// Tambah Pesanan Ajax
$("#tambahPesananForm").submit(function(e){
    e.preventDefault();

    var formDataPesanan = new FormData($(this)[0]);

    $.ajax({
        url: '/pesanan/tambah',
        type: 'POST',
        data: formDataPesanan,
        beforeSend: function(){
            $("#tambahPesananBtn").html("Menambah <i class='fas fa-spinner fa-spin'></i>");
        },
        success: function(data){
            if(data.message_pesanan == "success"){
                alertPesanan.addClass('alert-success');
                alertPesananMsg.html("Berhasil menambah pesanan!");
                alertPesanan.removeClass('d-none');
            }else{
                alertPesanan.addClass('alert-danger');
                alertPesananMsg.html("Terjadi Kesalahan!");
                alertPesanan.removeClass('d-none');
            }
            setTimeout(function(){
                location.reload();
            }, 1000);
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

// get Detail Pesanan
function getDetailPesanan(id){
    $.ajax({
        url: 'pesanan/detail/'+id,
        type: 'GET',
        beforeSend: function(){
            $("#listPesanan").children().remove();
            $("#totalHarga").children().remove();
            $("#loadingSpin").removeClass("d-none");
        },
        success: function(result){
            var namaMenus = result.data;
            var totalHarga = result.totalHarga;
            $("#totalHarga").html("<span>"+totalHarga+"</span>");
            $.each(namaMenus, function(index, data){
                $("#listPesanan").append('<li class="list-group-item">'+namaMenus[index].nama+'</li>');
            });
            $("#loadingSpin").addClass("d-none");
        }
    });
}

// get View Ubah Menu
function getViewUbahMenu(id){
    $.ajax({
        url: 'menu/ubah/'+id,
        type: 'GET',
        beforeSend: function(){
            $("#ubahMenuForm").hide();
            $("#namaMenu").val();
            $("#jenisMenu option").removeAttr('selected');
            $("#hargaMenu").val();
            $("#statusMenu option").removeAttr('selected');
            $("#loadingSpinMenu").removeClass("d-none");
        },
        success: function(result){
            $("#id_menu").val(result.id_menu);
            $("#namaMenu").val(result.nama);
            $("#jenisMenu option[value="+result.jenis+"]").attr('selected', 'selected');
            $("#hargaMenu").val(result.harga);
            $("#statusMenu option[value="+result.status+"]").attr('selected', 'selected');
            $("#ubahMenuForm").show();
            $("#loadingSpinMenu").addClass("d-none");
        }
    });
}

// Ubah Menu Ajax
$("#ubahMenuForm").submit(function(e){
    e.preventDefault();

    var formDataUbahMenu = new FormData($(this)[0]);

    $.ajax({
        url: '/menu/ubah',
        type: 'POST',
        data: formDataUbahMenu,
        beforeSend: function(){
            $("#ubahMenuBtn").html("Mengubah <i class='fas fa-spinner fa-spin'></i>");
        },
        success: function(data){
            if(data.status == "success"){
                alertUbahMenu.addClass('alert-success');
                alertUbahMenuMsg.html("Berhasil mengubah menu!");
                alertUbahMenu.removeClass('d-none');
            }else{
                alertUbahMenu.addClass('alert-danger');
                alertUbahMenuMsg.html("Terjadi Kesalahan");
                alertUbahMenu.removeClass('d-none');
            }
            setTimeout(function(){
                location.reload();
            }, 1000);
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

//Bind Url Hapus Menu
function getViewDeleteMenu(id){
    // $("#nama_menu").html(nama);
    $("#hapusBtn").attr('href', '/menu/hapus/'+id);
}

// get View Ubah Pesanan
function getViewUbahPesanan(id) {
    $.ajax({
        url: '/pesanan/ubah/'+id,
        type: 'GET',
        beforeSend: function(){
            $("#daftarMenu").find("input[type='checkbox']").removeAttr("checked");
            $("#detailPesanan").hide();
            $("#loadingSpinUbahPesanan").removeClass("d-none");
        },
        success: function(data){
            $("#pengguna").html(data.status_pesanan.nama_pengguna);
            $("#kategori_pengguna").html(data.status_pesanan.kategori_pengguna);
            $("#status_pesanan").html(data.status_pesanan.status_pesanan);
            $("#id_status_pesanan").val(data.status_pesanan.id_status_pesanan);
            $("#nomor_pesanan").val(data.status_pesanan.nomor_pesanan);
            $("#nomor_meja").val(data.status_pesanan.nomor_meja);

            var menuSelected = data.detail_pesanan;
            $.each(menuSelected, function(index, data){
                $("#id_detail_pesanan").val(menuSelected[index].id_detail_pesanan);
                $("#daftarMenu").find("input[type='checkbox'][value="+menuSelected[index].id_menu+"]").attr("checked", "checked");
            });

            $("#detailPesanan").show();
            $("#loadingSpinUbahPesanan").addClass("d-none");
        }
    });
}

// ubah Pesanan Action
$("#ubahPesananForm").submit(function(e){
    e.preventDefault();

    var formDataUbahPesanan = new FormData($(this)[0]);

    $.ajax({
        url: '/pesanan/ubah',
        type: 'POST',
        data: formDataUbahPesanan,
        beforeSend: function(data){
            $("#ubahPesananBtn").html('Mengubah <i class="fas fa-spinner fa-spin"></i>');
        },
        success: function(data){
            if(data.ubah_status == "success"){
                alertUbahPesanan.addClass('alert-success');
                alertUbahPesananMsg.html("Berhasil mengubah pesanan!");
                alertUbahPesanan.removeClass('d-none');
            }else{
                alertUbahPesanan.addClass('alert-danger');
                alertUbahPesananMsg.html("Terjadi Kesalahan!");
                alertUbahPesanan.removeClass('d-none');
            }
            setTimeout(function(){
                location.reload();
            }, 1000);
        },
        cache: false,
        processData: false,
        contentType: false
    });
});

// Bind Url Hapus Pesanan
function getViewDeletePesanan(id){
    $("#hapusPesananBtn").attr('href', '/pesanan/hapus/'+id);
}

</script>
