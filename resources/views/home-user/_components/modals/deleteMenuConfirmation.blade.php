<div class="modal fade" id="modalsDeleteMenuConfirmation">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="alert" class="alert alert-dismissible fade show d-none" role="alert">
                        <span id="alertMsg"></span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <h5 class="font-weight-bold">Anda yakin ingin menghapus menu <span id="nama_menu"></span> ?</h5>
                    <div class="row mt-4">
                        <div class="col-6">
                            <input type="hidden" name="id_menu" id="id_menu_delete">
                            <a href="" id="hapusBtn" class="btn btn-primary w-100">Hapus</a>
                        </div>
                        <div class="col-6">
                            <button type="button" data-dismiss="modal" class="btn btn-danger w-100">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
