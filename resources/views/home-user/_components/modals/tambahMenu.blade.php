<div class="modal fade" id="modalsTambahMenu">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div id="alert" class="alert alert-dismissible fade show d-none" role="alert">
                    <span id="alertMsg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <h5 class="font-weight-bold text-center">Tambah Menu</h5>
                <div class="mt-4">
                    <form action="/menu/tambah" id="tambahMenuForm" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Nama Menu</label>
                            <input type="text" class="form-control" name="nama">
                        </div>
                        <div class="form-group">
                            <label for="">Jenis Menu</label>
                            <select class="form-control" name="jenis">
                                <option value="makanan">Makanan</option>
                                <option value="minuman">Minuman</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Harga</label>
                            <input type="text" class="form-control" name="harga">
                        </div>
                        <div class="d-flex align-items-center float-right">
                            <button type="submit" id="tambahMenuBtn" class="btn btn-primary mr-2">Tambah</button>
                            <button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
