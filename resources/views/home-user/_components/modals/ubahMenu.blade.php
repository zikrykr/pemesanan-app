<div class="modal fade" id="modalsUbahMenu">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div id="alertUbahMenu" class="alert alert-dismissible fade show d-none" role="alert">
                    <span id="alertUbahMenuMsg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <h5 class="font-weight-bold text-center">Ubah Menu</h5>
                <div id="loadingSpinMenu" class="row mt-4 d-none">
                    <div class="col-12">
                        <div class="text-center">
                            Mengambil Data Menu <i class="fas fa-spinner fa-spin"></i>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <form action="/menu/ubah" id="ubahMenuForm" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_menu" id="id_menu">
                        <div class="form-group">
                            <label for="">Nama Menu</label>
                            <input type="text" class="form-control" id="namaMenu" name="nama">
                        </div>
                        <div class="form-group">
                            <label for="">Jenis Menu</label>
                            <select class="form-control" id="jenisMenu" name="jenis">
                                <option value="makanan">Makanan</option>
                                <option value="minuman">Minuman</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Harga</label>
                            <input type="text" class="form-control" id="hargaMenu" name="harga">
                        </div>
                        <div class="form-group">
                            <label for="">Status</label>
                            <select class="form-control" id="statusMenu" name="status">
                                <option value="ready">Ready</option>
                                <option value="not_ready">Not Ready</option>
                            </select>
                        </div>
                        <div class="d-flex align-items-center float-right">
                            <button type="submit" id="ubahMenuBtn" class="btn btn-warning mr-2">Ubah</button>
                            <button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
