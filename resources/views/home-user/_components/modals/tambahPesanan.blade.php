<div class="modal fade" id="modalsTambahPesanan">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="alertPesanan" class="alert alert-dismissible fade show d-none" role="alert">
                        <span id="alertPesananMsg"></span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <h5 class="font-weight-bold text-center">Tambah Pesanan</h5>
                    <div class="mt-4">
                        <form action="/pesanan/tambah" id="tambahPesananForm" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="">Nomor Meja</label>
                                <input type="text" class="form-control" name="nomor_meja">
                            </div>
                            <input type="hidden" name="id_pengguna" value="{{ Session::get('id_pengguna') }}">
                            <div class="form-group mt-4">
                                <label for=""><b>Daftar Menu</b></label>
                                <div class="row mt-1">
                                    <div class="col-6">
                                        <label for="">Makanan</label>
                                        @if ($menus->isEmpty())
                                            Daftar Makanan Kosong
                                        @else
                                            @foreach ($menus as $menu)
                                                @if ($menu->status == "ready")
                                                    @if ($menu->jenis == "makanan")
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value="{{ $menu->id }}" name="id_menu[]">
                                                            <label class="form-check-label" for="defaultCheck1">
                                                                {{ $menu->nama }}
                                                            </label>
                                                        </div>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-6">
                                        <label for="">Minuman</label>
                                        @if ($menus->isEmpty())
                                            Daftar Minuman Kosong
                                        @else
                                            @foreach ($menus as $menu)
                                                @if ($menu->status == "ready")
                                                    @if ($menu->jenis == "minuman")
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value="{{ $menu->id }}" name="id_menu[]">
                                                            <label class="form-check-label" for="defaultCheck1">
                                                                {{ $menu->nama }}
                                                            </label>
                                                        </div>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex align-items-center float-right mt-4">
                                <button type="submit" id="tambahPesananBtn" class="btn btn-primary mr-2">Tambah</button>
                                <button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
