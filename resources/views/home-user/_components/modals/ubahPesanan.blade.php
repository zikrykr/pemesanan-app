<div class="modal fade" id="modalsUbahPesanan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div id="alertUbahPesanan" class="alert alert-dismissible fade show d-none" role="alert">
                    <span id="alertUbahPesananMsg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <h5 class="font-weight-bold text-center">Ubah Pesanan</h5>
                <div id="loadingSpinUbahPesanan" class="row mt-4 d-none">
                    <div class="col-12">
                        <div class="text-center">
                            Mengambil Data Pesanan <i class="fas fa-spinner fa-spin"></i>
                        </div>
                    </div>
                </div>
                <div class="mt-4" id="detailPesanan">
                    <div class="row">
                        <div class="col-12 d-flex align-items-center justify-content-between">
                            <p class="mb-3 font-weight-bold"><span id="pengguna"></span> ( <span id="kategori_pengguna"></span> )</p>
                            <p>Status Pesanan : <span id="status_pesanan"></span></p>
                        </div>
                    </div>
                    <form action="/pesanan/ubah" id="ubahPesananForm" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_status_pesanan" id="id_status_pesanan">
                        <div class="form-group">
                            <label for="">Nomor Pesanan</label>
                            <input type="text" class="form-control" id="nomor_pesanan" name="nomor_pesanan" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Nomor Meja</label>
                            <input type="text" class="form-control" id="nomor_meja" name="nomor_meja">
                        </div>
                        <div class="form-group mt-4">
                            <label for=""><b>Daftar Menu</b></label>
                            <div class="row mt-1" id="daftarMenu">
                                <div class="col-6">
                                    <label for="">Makanan</label>
                                    @if ($menus->isEmpty())
                                        Daftar Makanan Kosong
                                    @else
                                        @foreach ($menus as $menu)
                                            @if ($menu->status == "ready")
                                                @if ($menu->jenis == "makanan")
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value="{{ $menu->id }}" name="id_menu[]">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            {{ $menu->nama }}
                                                        </label>
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <div class="col-6">
                                    <label for="">Minuman</label>
                                    @if ($menus->isEmpty())
                                        Daftar Minuman Kosong
                                    @else
                                        @foreach ($menus as $menu)
                                            @if ($menu->status == "ready")
                                                @if ($menu->jenis == "minuman")
                                                    <div class="form-check">
                                                        {{-- <input type="text" name="id_detail_pesanan[]" id="id_detail_pesanan"> --}}
                                                        <input class="form-check-input" type="checkbox" value="{{ $menu->id }}" name="id_menu[]">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            {{ $menu->nama }}
                                                        </label>
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-center float-right mt-4">
                            <button type="submit" id="ubahPesananBtn" class="btn btn-warning mr-2">Ubah</button>
                            <button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
