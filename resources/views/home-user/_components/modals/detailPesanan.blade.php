<div class="modal fade" id="modalsDetailPesanan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="font-weight-bold text-center">Detail Pesanan</h5>
                <div class="mt-4">
                    <div class="form-group mt-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex align-items-center justify-content-between">
                                    <label for="">Daftar Pesanan</label>
                                    <div class="d-block">
                                        <p class="mb-0">Total Harga</p>
                                        <h6 class="font-weight-bold" id="totalHarga"></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="list-group mt-4" id="listPesanan">
                        </ul>
                        <div id="loadingSpin" class="row d-none">
                            <div class="col-12">
                                <div class="text-center">
                                    Mengambil Data Pesanan <i class="fas fa-spinner fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex align-items-center float-right mt-4">
                        <button type="button" data-dismiss="modal" class="btn btn-danger">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
