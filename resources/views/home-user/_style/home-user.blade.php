<style>
    .table-toggle{
        padding: 0.7em;
        border-radius: 50%;
    }
    .table-toggle:hover,
    .table-toggle:focus{
        background: #ccc;
        cursor: pointer;
    }
</style>
