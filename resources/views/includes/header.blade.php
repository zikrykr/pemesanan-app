<title>@yield('title')</title>
<meta name="description" content="@yield('description')">
<meta name="keywords" content="@yield('keywords')" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="geo.region" content="ID" />
<meta name="robots" content="index" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#7ac70c">
<meta name="csrf-token" content="{{ csrf_token() }}" />
