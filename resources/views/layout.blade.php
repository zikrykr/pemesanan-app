<!DOCTYPE html>
<html>
	<head>
		{{-- head --}}
		@include('includes.header')

		{{-- css --}}
		@include('includes.css')

		{{-- yield css--}}
		@yield('css')

		{{-- include js --}}
		@include('includes.footer')
	</head>
	<body>

		<div id="wrapper">
			{{-- yield content --}}
			@yield('content')

			{{-- yield js--}}
			@yield('js')
		</section>
	</body>
</html>
