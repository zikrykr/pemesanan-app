<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePesanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesanan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor_meja');
            $table->string('nomor_pesanan');
            $table->foreign('nomor_pesanan')->references('nomor_pesanan')->on('status_pesanan');
            $table->bigInteger('id_menu')->unsigned();
            $table->foreign('id_menu')->references('id')->on('menu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanan');
        Schema::enableForeignKeyConstraints();
    }
}
